import { Component } from '@angular/core';
import { ResultatService } from '../services/resultat.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-resultat',
  templateUrl: './resultat.component.html',
  styleUrls: ['./resultat.component.css']
})
export class ResultatComponent {
  selectedPokemon: any;
  stats:any[]=[];
  stat!:Stat;
  textStatsColor: any;
  textEvolutionsColor:any;
  textMovesColor:any;
  pokemonColor:any;
  btnStatsColor: any;
  btnEvolutionsColor: any;
  btnMovesColor: any;
  showStats: Boolean=false;
  showEvolutions: Boolean=false;
  showMoves: Boolean=false;
  typeValue!: TypeValue;
  typeColor:any;
  descriptionURL:any;
  description:any;
  typeNames: any[] = [];
  constructor(private Service: ResultatService,private route: ActivatedRoute, private router:Router) { }
  pokemonList: any[] = [];
  pokemon: any;
  imageUrl:any;
  ngOnInit() {
    document.documentElement.style.setProperty
    ('--wbColer', "red");
    this.route.queryParams.subscribe(params => {
      this.pokemon = params['pokemon'];
      this.getPokemonDetailsByName(this.pokemon);
      console.log("cooolloer"+this.pokemonColor);
    });
  }

  getPokemonList() {
    this.Service.getPokemonList().subscribe(
      (data: any) => {
        this.pokemonList = data.results;
        console.log(this.pokemonList)
      },
      error => {
        console.error('Error fetching Pokemon list:', error);
      }
    );
  }
  getPokemonDetails(id: number) {
    this.Service.getPokemonDetails(id).subscribe(
      (data: any) => {
        this.selectedPokemon = data;
      },
      error => {
        console.error('Error fetching Pokemon details:', error);
      }
    );
  }
  getPokemonDetailsByName(name: string) {
    this.Service.getPokemonDetailsByName(name).subscribe(
      (data: any) => {
        console.log(name);
        console.log(data);
        this.selectedPokemon = data;
      this.imageUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.selectedPokemon.id}.png`;
        switch (this.selectedPokemon.types[0].type.name) {
          case "grass":
            document.documentElement.style.setProperty
            ('--wbColer', "#88BE5D");
            this.pokemonColor="#88BE5D";
              break;
          case "electric":
            document.documentElement.style.setProperty
            ('--wbColer', "#F7CF43");
            this.pokemonColor="#F7CF43";
              break;
          case "poison":
            document.documentElement.style.setProperty
            ('--wbColer', "#B563CE");  
            this.pokemonColor="#B563CE";
            break;
         
      }
      for (var type of this.selectedPokemon.types){
        switch(type.type.name){
          case "grass": this.typeColor="#88BE5D";break;
          case "electric": this.typeColor="#F7CF43";break;
          case "poison": this.typeColor="#B563CE";break;

        }
        this.typeValue = new TypeValue(type.type.name,this.typeColor);
        this.typeNames.push(this.typeValue);
      }
      for(var statIterator of this.selectedPokemon.stats){
        this.stat=new Stat(statIterator.stat.name,statIterator.base_stat);
        this.stats.push(this.stat);
      }
      this.descriptionURL= this.selectedPokemon.species.url;
      this.Service.getPokemonDescription(this.descriptionURL).subscribe(
        (data: any) => {
          console.log(data);
          this.description = data.flavor_text_entries[6].flavor_text;
        },
        error => {
          console.error('Error fetching Pokemon details:', error);
        }
      )
      console.log(this.typeNames);
      },
      error => {
        this.router.navigate(['/error']);

      }
    );
  }
  onClickStats(){
    this.btnStatsColor=this.pokemonColor;
    this.textStatsColor="white";
    this.btnEvolutionsColor="white";
    this.textEvolutionsColor=this.pokemonColor;
    this.btnMovesColor="white";
    this.textMovesColor=this.pokemonColor;
    this.showStats=true;
    this.showMoves=false;
    this.showEvolutions=false;
  }

  onClickEvolutions(){
    this.btnEvolutionsColor=this.pokemonColor;
    this.textEvolutionsColor="white";
    this.btnStatsColor="white";
    this.textStatsColor=this.pokemonColor;
    this.btnMovesColor="white";
    this.textMovesColor=this.pokemonColor;
    this.showEvolutions=true;
    this.showMoves=false;
    this.showStats=false;

  }

  onClickMoves(){
    this.btnMovesColor=this.pokemonColor;
    this.textMovesColor="white";
    this.btnStatsColor="white";
    this.textStatsColor=this.pokemonColor;
    this.btnEvolutionsColor="white";
    this.textEvolutionsColor=this.pokemonColor;
    this.showMoves=true;
    this.showEvolutions=false;
    this.showStats=false;


  }
}

export class TypeValue {
  constructor(public name: string, public color: string) {}
}


export class Stat{
  constructor(public name: string, public base: string) {}
}